import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FirstchildRoutingModule } from './firstchild-routing.module';
import { FirstchildComponent } from './firstchild.component';


@NgModule({
  declarations: [
    FirstchildComponent
  ],
  imports: [
    CommonModule,
    FirstchildRoutingModule
  ]
})
export class FirstchildModule { }
