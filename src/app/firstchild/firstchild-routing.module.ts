import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FirstchildComponent } from './firstchild.component';

const routes: Routes = [{ path: '', component: FirstchildComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FirstchildRoutingModule { }
