import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FirstComponent } from './first.component';

const routes: Routes = [
  { path: '', component: FirstComponent },
  {
    path: 'firstchild',
    data: { preload: false },
    loadChildren: () =>
      import('../firstchild/firstchild.module').then((m) => m.FirstchildModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FirstRoutingModule {}
