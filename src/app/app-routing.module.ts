import { CustompreloadService } from './customepreload.service';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'first',
    data: { preload: true },
    loadChildren: () =>
      import('./first/first.module').then((m) => m.FirstModule),
  },
  {
    path: 'second',
    loadChildren: () =>
      import('./second/second.module').then((m) => m.SecondModule),
  },
  {
    path: 'third',
    loadChildren: () =>
      import('./third/third.module').then((m) => m.ThirdModule),
  },
  {
    path: 'firstchild',
    loadChildren: () =>
      import('./firstchild/firstchild.module').then((m) => m.FirstchildModule),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: CustompreloadService,
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
